const host = 'https://qa-interview.srcli.xyz'
const username = 'root'
const password = 'root123'

context('/data endpoint', () => {
    beforeEach(() => cy.clearCookies())

    it('request GET /data endpoint - has logged in', () => {
        // set cookie session_id to indicate a user has logged in
        cy.setCookie('username', username)
        cy.getCookie('username').should('exist')

        cy.request(`${host}/data`)
            .should((response) => {
                expect(response.status).to.eq(200)
            })
        cy.visit(`${host}/data`)

        // expect to have 2 titles for income and outcome
        cy.get('body').find('h1').should('have.length', 2)
        cy.get('body').find('h1').eq(0)
            .then($h1 => expect($h1.text()).to.eq('Pemasukan'))
        cy.get('body').find('h1').eq(1)
            .then($h1 => expect($h1.text()).to.eq('Pengeluaran'))

        // expect to have 2 tables with 10 data rows per table
        cy.get('body').find('table').should('have.length', 2)
        // got 11 rows due to the first row is a table header
        // income rows
        cy.get('body > table').eq(0).find('tr').should('have.length', 11)
        // outcome rows
        cy.get('body > table').eq(1).find('tr').should('have.length', 11)
        // expect to have start and end timestamp form to filter
        cy.get('body').find('form > input[name=start]').should('be.visible')
        cy.get('body').find('form > input[name=end]').should('be.visible')
    })

    it('request GET /data endpoint - has not logged in', () => {
        cy.request({
            url: `${host}/data`,
            followRedirect: false, // turn off following redirects
        }).then((resp) => {
            expect(resp.status).to.eq(302)
        })
    })

    it('request POST /data endpoint to filter with valid start/end range - has logged in', () => {
        // set cookie session_id to indicate a user has logged in
        cy.setCookie('username', username)
        cy.getCookie('username').should('exist')

        cy.get('input[name=start]')
            .type('2018-07-06').should('have.value', '2018-07-06')
        cy.get('input[name=end]')
            .type('2018-07-07').should('have.value', '2018-07-07')

        cy.get('input[type=submit]').click()
            .then(() => {
                cy.request({
                    url: `${host}/filter`,
                    method: 'POST',
                    followRedirect: false, // turn off following redirects
                    failOnStatusCode: false,
                    form: true,
                    body: {
                        start: '2018-07-06',
                        end: '2018-07-07',
                    },
                }).then((resp) => {
                    expect(resp.status).to.eq(302)
                })
            })
        cy.url().should('eq', `${host}/data`)

        // expect 5 data rows to be shown once filter applied:
        // got 6 rows due to the first row is a table header

        // Pemasukan
        // TimeStamp	Deskripsi	Jumlah
        // 2018-07-07	pemasukan7	7
        // 2018-07-07	pemasukan7	7
        // 2018-07-07	pemasukan7	7
        // 2018-07-07	pemasukan7	7
        // 2018-07-07	pemasukan7	7
        cy.get('body > table').eq(0).find('tr').should('have.length', 6)
        // Pengeluaran
        // TimeStamp	Deskripsi	Jumlah
        // 2018-07-07	pengeluaran7	7
        // 2018-07-07	pengeluaran7	7
        // 2018-07-07	pengeluaran7	7
        // 2018-07-07	pengeluaran7	7
        // 2018-07-07	pengeluaran7	7
        cy.get('body > table').eq(1).find('tr').should('have.length', 6)
    })

    it('request POST /data endpoint to filter with start > end - has logged in', () => {
        // set cookie session_id to indicate a user has logged in
        cy.setCookie('username', username)
        cy.getCookie('username').should('exist')

        cy.get('input[name=start]')
            .type('2018-07-08').should('have.value', '2018-07-08')
        cy.get('input[name=end]')
            .type('2018-07-07').should('have.value', '2018-07-07')

        cy.get('input[type=submit]').click()
            .then(() => {
                cy.request({
                    url: `${host}/filter`,
                    method: 'POST',
                    followRedirect: false, // turn off following redirects
                    failOnStatusCode: false,
                    form: true,
                    body: {
                        start: '2018-07-08',
                        end: '2018-07-07',
                    },
                }).then((resp) => {
                    // expect to get 4xx response if start > end timestamp
                    expect(resp.status).to.eq(405)
                    expect(resp.body).to.eq('Filter Parameter are wrong\n')
                })
            })
    })

    it('request POST /data endpoint to filter - has not logged in', () => {
        cy.request({
            url: `${host}/filter`,
            method: 'POST',
            followRedirect: false, // turn off following redirects
            failOnStatusCode: false,
            form: true,
            body: {
                start: '2018-07-06',
                end: '2018-07-07',
            },
        }).then((resp) => {
            expect(resp.status).to.eq(302)
        })
    })
})