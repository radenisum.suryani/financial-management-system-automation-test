const host = 'https://qa-interview.srcli.xyz'
const validUsername = 'root'
const validPassword = 'root123'
const invalidPassword = 'root12345'

context('/login endpoint', () => {
  beforeEach(() => {
    cy.visit(`${host}/login`)
    cy.clearCookies()
  })

  it('request GET /login endpoint - has logged in', () => {
    // submit login action to set cookie session_id to indicate a user has logged in
    cy.get('input[name=username]')
      .type(validUsername).should('have.value', validUsername)
    cy.get('input[name=password]')
      .type(validPassword).should('have.value', validPassword)

    cy.get('input[type=submit]').click()
      .then(() => {
        cy.request({
          url: `${host}/login`,
          followRedirect: false, // turn off following redirects
        }).then((resp) => {
          expect(resp.status).to.eq(302)
        })
      })
    cy.getCookie('username').should('exist')
    // redirected to home page
    cy.url().should('eq', `${host}/`)
  })

  it('request GET /login endpoint - has not logged in', () => {
    cy.request(`${host}/login`)
      .should((response) => {
        expect(response.status).to.eq(200)
      })
    cy.get('input[name=username]').should('be.visible')
    cy.get('input[name=password]').should('be.visible')
  })

  it('request POST /login endpoint - has logged in', () => {
    // submit login action to set cookie session_id to indicate a user has logged in
    cy.get('input[name=username]')
      .type(validUsername).should('have.value', validUsername)
    cy.get('input[name=password]')
      .type(validPassword).should('have.value', validPassword)

    cy.get('input[type=submit]').click()
      .then(() => {
        cy.request({
          url: `${host}/login`,
          method: 'POST',
          followRedirect: false, // turn off following redirects
        }).then((resp) => {
          expect(resp.status).to.eq(302)
        })
      })
    cy.getCookie('username').should('exist')
    // redirected to home page
    cy.url().should('eq', `${host}/`)
  })

  it('request POST /login endpoint with valid username & password - has not logged in', () => {
    // submit login action to set cookie session_id to indicate a user has logged in
    cy.get('input[name=username]')
      .type(validUsername).should('have.value', validUsername)
    cy.get('input[name=password]')
      .type(validPassword).should('have.value', validPassword)

    cy.get('input[type=submit]').click()
      .then(() => {
        cy.request({
          url: `${host}/login`,
          method: 'POST',
          followRedirect: false, // turn off following redirects
          form: true,
          body: {
            username: validUsername,
            password: validPassword,
          },
        }).then((resp) => {
          expect(resp.status).to.eq(302)
        })
      })
    cy.getCookie('username').should('exist')
    // redirected to home page
    cy.url().should('eq', `${host}/`)
  })

  it('request POST /login endpoint with invalid username & password - has not logged in', () => {
    cy.get('input[name=username]')
      .type(validUsername).should('have.value', validUsername)
    cy.get('input[name=password]')
      .type(invalidPassword).should('have.value', invalidPassword)

    cy.get('input[type=submit]').click()
      .then(() => {
        cy.request({
          url: `${host}/login`,
          method: 'POST',
          form: true,
          body: {
            username: validUsername,
            password: invalidPassword,
          },
          failOnStatusCode: false,
        }).then((resp) => {
          expect(resp.status).to.eq(401)
          expect(resp.body).to.eq('The password or username is wrong\n')
        })
      })
    // redirected to home page
    cy.url().should('eq', `${host}/login`)
  })
})