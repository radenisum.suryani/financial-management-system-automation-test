const host = 'https://qa-interview.srcli.xyz'
const notFoundURL = 'https://qa-interview.srcli.xyz/not-found-url'

context('/invalid endpoint', () => {
    it('404 `not found` response', () => {
        cy.request({
            url: notFoundURL,
            followRedirect: false,
            failOnStatusCode: false,
        }).then((resp) => {
            expect(resp.status).to.eq(404)
            expect(resp.redirectedToUrl).to.eq(undefined)
            expect(resp.body).to.eq('Page not found\n')
        })
    })
})