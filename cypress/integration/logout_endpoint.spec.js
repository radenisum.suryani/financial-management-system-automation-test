const host = 'https://qa-interview.srcli.xyz'

context('/logout endpoint', () => {
    it('request POST /logout endpoint', () => {
        cy.request({
            url: `${host}/logout`,
            method: 'POST',
            followRedirect: false, // turn off following redirects
        }).then((resp) => {
            expect(resp.status).to.eq(302)
        })
        // redirected to login page
        cy.url().should('eq', `${host}/login`)
        cy.getCookies().should('be.empty')
    })
})