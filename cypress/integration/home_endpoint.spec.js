const host = 'https://qa-interview.srcli.xyz'

context('/ GET endpoint', () => {
  beforeEach(() => cy.visit(host))

  it('request home page', () => {
    cy.request(host)
      .should((response) => {
        expect(response.status).to.eq(200)
      })
    cy.get('h1').should('have.text', 'Welcome!\n')
  })
})